package com.example.puzzlen;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.List;

public class Adaptador extends BaseAdapter {
    private Context context;
    List<Uri>listaImagenes;

    /*public int[] imagenesLista = {
            R.drawable.girasoles,
            R.drawable.tigre,
            R.drawable.tigre_1,
            R.drawable.nieve,
    };*/

    public Adaptador(Context context, List<Uri> listaImagenes) {

        this.context = context;
        this.listaImagenes=listaImagenes;
    }

    @Override
    public int getCount() {
       // return imagenesLista.length;
        return listaImagenes.size();

    }

    @Override
    public Object getItem(int position) {
       // return imagenesLista[position];
        return listaImagenes.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        //imageView.setImageResource(imagenesLista[position]);
        imageView.setImageURI(listaImagenes.get(position));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(
                new ViewGroup.LayoutParams(
                        200,
                        210
                )
        );
        return imageView;
    }
}
