package com.example.puzzlen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Game_over extends AppCompatActivity {

    //private String context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
    }

    public void nuevaPartida(View v) {

        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);

    }

    public void exit(View v) {
       //this.finish();
       Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP) | (Intent.FLAG_ACTIVITY_NEW_TASK));
        startActivity(intent);
        //System.exit(0);
       /* Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/
        /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);*/
        /*Intent intent = new Intent((Intent.ACTION_MAIN));
        finish();*/

    }
}
