package com.example.puzzlen;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import com.example.puzzlen.Juego;
import com.example.puzzlen.MainActivity;

/*
    Esta clase representa el servicio que se va a encargar de
    gestionar la reproducción de la música de fondo de la aplicación.
 */
public class ServicioMusica extends Service {

    private final IBinder mBinder = new ServiceBinder();
    MediaPlayer mPlayer,mContador;
    private int length = 0;

    public ServicioMusica() {
    }

    public class ServiceBinder extends Binder {
        ServicioMusica getService() {
            return ServicioMusica.this;
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
            mPlayer = MediaPlayer.create(this, R.raw.musica);

            //mContador=MediaPlayer.create(this,R.raw.crono);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mPlayer != null) {
            mPlayer.start();
        }
        return START_NOT_STICKY;
    }

    // Este método pausa la música
    public void pauseMusic() {
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.pause();
                length = mPlayer.getCurrentPosition();
            }
        }
    }
    // Este método reinicia la reproducción de la música
    public void resumeMusic() {
        if (mPlayer != null) {
            if (!mPlayer.isPlaying()) {
                mPlayer.seekTo(length);
                mPlayer.start();
            }
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPlayer != null) {
            try {
                mPlayer.stop();
                mPlayer.release();
            } finally {
                mPlayer = null;
            }
        }
    }
}