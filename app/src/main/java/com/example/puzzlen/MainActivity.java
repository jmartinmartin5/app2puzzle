package com.example.puzzlen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    private boolean favorito = false;
    EditText nombre;

    SoundPool soundPool;
    int click;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Puzzle);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombre = (EditText) findViewById(R.id.jugador);
       // mediaPlayer = MediaPlayer.create(this, R.raw.click);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(6)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        }

        click = soundPool.load(this, R.raw.click, 1);


    }

    private void setFavoriteIcon(MenuItem item) {

        int id = this.favorito ? R.drawable.ic_baseline_favorite_24 : R.drawable.ic_baseline_favorite_border_24;
        item.setIcon(ContextCompat.getDrawable((Context)this, id));
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.favorito:
                this.favorito = !this.favorito;
                this.setFavoriteIcon(item);
               // mediaPlayer.start();
                soundPool.play(click,5,5,1,0,1);
                break;

            case R.id.ayuda:
                soundPool.play(click,2,2,1,0,1);
                //mediaPlayer.start();
                Toast.makeText(this.getApplicationContext(), (CharSequence)"Ayuda",Toast.LENGTH_LONG).show();
                WebView myWebView = new WebView(this.getApplicationContext());
                this.setContentView((View)myWebView);
                myWebView.loadUrl("file:///android_asset/juego.html");
                break;

            case R.id.salir:
                soundPool.play(click,5,5,1,0,1);
                // mediaPlayer.start();
                System.exit(0);
                break;
            case R.id.compartir:
                soundPool.play(click,2,2,1,0,1);
                //mediaPlayer.start();
                Intent send = new Intent();
                send.putExtra("android.intent.extra.TEXT", "https://gitlab.com/MonchoUOC/puzzle.git");
                send.setType("text/plain");
                send = Intent.createChooser(send, (CharSequence)null);
                this.startActivity(send);
                break;
            case R.id.puntuacion:
                soundPool.play(click,2,2,1,0,1);
                //mediaPlayer.start();
                Intent myIntent = new Intent(this, Ranking.class);
                startActivity(myIntent);

                break;



            default:
                soundPool.play(click,2,2,1,0,1);
                //mediaPlayer.start();
                super.onOptionsItemSelected(item);
        }
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barra, menu);
        return true;
    }

    public void exit(View v){
        soundPool.play(click,1,1,1,0,1);
        //mediaPlayer.start();
        System.exit(0);
    }

    public void imagenesJuego(View v) {
        soundPool.play(click,1,1,1,0,1);
        //mediaPlayer.start();
        Intent intent = new Intent((Context)this, SeleccionaImagen.class);
        intent.putExtra("nombrejugador",nombre.getText().toString());
        this.startActivity(intent);
    }

    public void  onBackPressed() {
        super.onBackPressed();
        Intent  intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }



}

