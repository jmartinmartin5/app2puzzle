package com.example.puzzlen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Ranking extends AppCompatActivity {
    private SimpleDateFormat sdf;
    private String fechaActual;
    private String patronFecha = "dd/MM/yyyy";
    private int CALENDAR_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);


        // Solicitamos los permisos de escritura y lectura en el Calendario.
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, CALENDAR_PERMISSION_CODE);


        TextView ranking = findViewById(R.id.ranking);



        // Comprobamos si se han concedido los permisos de lectura y escritura en el Calendario.
        if (ContextCompat.checkSelfPermission(Ranking.this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(Ranking.this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            obtenerPuntuaciones(ranking); //Con los permisos concedidos, se muestran las puntuaciones en el TextView.
        } else {
            //Sin la concesión del permiso, se muestra el siguiente mensaje en el TextView.
           // ranking.append("Debe dar permisos de lectura y escritura al Calendario para poder visualizar y registrar nuevas puntuaciones.");
        }



        BDHelper bdHelper= new BDHelper(this);
        SQLiteDatabase sqLiteDatabase=bdHelper.getReadableDatabase();

        // Construimos la consulta a la BBDD
        String[] projection = {

                BBDDEsquema.NOMBRE,
                BBDDEsquema.FECHA,
                BBDDEsquema.NIVEL,
                "MIN(" + BBDDEsquema.PUNTOS + ")"
        };

        Cursor cursor = sqLiteDatabase.query(
                BBDDEsquema.NOM_TABLA,   // Tabla a consultar
                projection,                 // Las columnas que queremos que devuelva
                null,
                null,
                BBDDEsquema.NIVEL,  // Agrupamos por nivel
                null,
                null
        );

        // Pintamos las puntuaciones
        while(cursor.moveToNext()) {
            ranking.append(cursor.getString(0) + "   " + cursor.getString(2) + "   " + cursor.getString(1) + "   " + String.format("%.2f", cursor.getDouble(3)).replace(".", ",")  +"\n");
        }
        cursor.close();
    }

    private void obtenerPuntuaciones(TextView ranking) {
        ranking.append("");

        for (int i = 0; i < 5; i++) {
            ContentResolver contentResolver = getContentResolver();
            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();

            Calendar beginTime = Calendar.getInstance();
            beginTime.set(2000, Calendar.JANUARY, 1, 0, 0);
            long startMills = beginTime.getTimeInMillis();
            long endMills = System.currentTimeMillis();

            String titulo = "App2Puzzle - ¡Nuevo récord Nivel" + (i + 1) + "!";
            ContentUris.appendId(builder, startMills);
            ContentUris.appendId(builder, endMills);
            String[] args = new String[]{"3", titulo};

            // Seleccionamos del Calendario aquellas entradas que coinciden con las puntuaciones de nuestro juego
            Cursor eventCursor = contentResolver.query(builder.build(), new String[]{CalendarContract.Instances.TITLE,
                            CalendarContract.Instances.BEGIN, CalendarContract.Instances.END, CalendarContract.Instances.DESCRIPTION},
                    CalendarContract.Instances.CALENDAR_ID + " = ? AND " + CalendarContract.Instances.TITLE + " = ?", args, null);

            if (eventCursor.getCount() > 0) {
                double min = Double.MAX_VALUE;
                Date minDate = new Date();

                while (eventCursor.moveToNext()) {
                    final String title = eventCursor.getString(0);
                    final Date begin = new Date(eventCursor.getLong(1));
                    final Date end = new Date(eventCursor.getLong(2));
                    final String description = eventCursor.getString(3);

                    // Nos quedamos con la puntuación más baja
                    if (Double.parseDouble(description.replace(",", ".")) < min) {
                        min = Double.parseDouble(description.replace(",", "."));
                        minDate = begin;
                    }
                }

                sdf = new SimpleDateFormat(patronFecha);
                fechaActual = sdf.format(minDate);

                ranking.append((i + 1) + "     " + fechaActual + " " + String.format("%.2f", min).replace(".", ",") + "\n");
            }
        }
    }

}
