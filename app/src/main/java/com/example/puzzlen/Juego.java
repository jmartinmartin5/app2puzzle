package com.example.puzzlen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.CalendarContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static java.lang.Math.abs;

public class Juego extends AppCompatActivity {

    private ArrayList<ColocaPiezas> piezas;
    int numeroPiezas;
    int position;
    int filas;
    Contador contador;
    MediaPlayer mp;
    MediaPlayer mpContador;
    boolean reproduciendo = false;
    private long tiempoInicio, tiempoFin, tiempoDiferencia;
    private double segTranscurridos;
    private String nombre= "Juanma";
    private String fechaActual;
    private String patronFecha = "dd/MM/yyyy";
    private SimpleDateFormat sdf;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        tiempoInicio = System.currentTimeMillis();
        List<Uri> lista = (List<Uri>)getIntent().getSerializableExtra("milista");

        Intent i = getIntent(); // Recibe la imagen de la clase SeleccionImagen
        position = i.getExtras().getInt("id");


        Adaptador imageAdapter = new Adaptador(this, lista);
        MuevePiezas muevePiezas = new MuevePiezas(Juego.this);


        final RelativeLayout layout = findViewById(R.id.layout2);
        final ImageView imageView = (ImageView) findViewById(R.id.imagen);
        //imageView.setImageResource(imageAdapter.imagenesLista[position]);
        imageView.setImageURI(imageAdapter.listaImagenes.get(position));


        imageView.post(new Runnable() {
            @Override
            public void run() {


                piezas = recorteImagen();

                Collections.shuffle(piezas);
                for (ColocaPiezas pieza : piezas) {
                    pieza.setOnTouchListener(muevePiezas);
                    layout.addView(pieza);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) pieza.getLayoutParams();
                    params.leftMargin = new Random().nextInt(layout.getWidth() - pieza.pieceWidth);
                    params.topMargin = layout.getHeight() - pieza.pieceHeight;
                    pieza.setLayoutParams(params);
                }
            }
        });
    }


    //Función que recorta  y da forma a la imagen que se crea en el puzzle y la coloca en un Array
    private ArrayList<ColocaPiezas> recorteImagen() {
        Bundle extras = getIntent().getExtras();
        filas = extras.getInt("filas");


        if (filas == 0){
            filas=3;
        }else{
            filas = extras.getInt("filas");
        }
        int contadorAumenta;

        //int filas = extras.getInt("filas");
        int columnas = filas;
        numeroPiezas = (int) Math.pow(filas,2);
        nombre=extras.getString("nombreJugador");

        mpContador = MediaPlayer.create(this, R.raw.crono);


        contadorAumenta = numeroPiezas;
        if (contadorAumenta == 9) {
            contador = new Contador(30000, 1000);
            contador.start();
            mpContador.setLooping(true);
            mpContador.start();
        } else if (contadorAumenta == 16) {
            contador = new Contador(60000, 1000);
            contador.start();
            mpContador.setLooping(true);
            mpContador.start();
        } else if (contadorAumenta == 25) {
            contador = new Contador(120000, 1000);
            contador.start();
            mpContador.setLooping(true);
            mpContador.start();
        }

        ImageView imageView = findViewById(R.id.imagen);
        ArrayList<ColocaPiezas> piezas = new ArrayList<>(numeroPiezas);

        //Mapa de bits de la imagen original
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        int[] dimensiones = getBitmapPositionInsideImageView(imageView);
        int escalaBitmapIzquierda = dimensiones[0];
        int escalaBitmapArriba = dimensiones[1];
        int escalaBitmapAncho = dimensiones[2];
        int scaledBitmapHeight = dimensiones[3];

        int recorteImageAncho = escalaBitmapAncho - 2 * abs(escalaBitmapIzquierda);
        int recorteImageAlto = scaledBitmapHeight - 2 * abs(escalaBitmapArriba);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, escalaBitmapAncho, scaledBitmapHeight, true);
        Bitmap croppedBitmap = Bitmap.createBitmap(scaledBitmap, abs(escalaBitmapIzquierda), abs(escalaBitmapArriba), recorteImageAncho, recorteImageAlto);


        // Calcular el ancho y alto  de las piezas
        int piezaAncho = recorteImageAncho / columnas;
        int piezaAlto = recorteImageAlto / filas;

        // Crear cada pieza de mapa de bits y la añade a la matriz resultante
        int yCoord = 0;
        for (int fila = 0; fila < columnas; fila++) {
            int xCoord = 0;
            for (int col = 0; col < filas; col++) {
                // calculate offset for each piece
                int offsetX = 0;
                int offsetY = 0;
                if (col > 0) {
                    offsetX = piezaAncho / 3;
                }
                if (fila > 0) {
                    offsetY = piezaAlto / 3;
                }

                Bitmap pieceBitmap = Bitmap.createBitmap(croppedBitmap, xCoord - offsetX, yCoord - offsetY, piezaAncho + offsetX, piezaAlto + offsetY);
                ColocaPiezas piece = new ColocaPiezas(getApplicationContext());
                piece.setImageBitmap(pieceBitmap);
                piece.xCoord = xCoord - offsetX + imageView.getLeft();
                piece.yCoord = yCoord - offsetY + imageView.getTop();
                piece.pieceWidth = piezaAncho + offsetX;
                piece.pieceHeight = piezaAlto + offsetY;

                //  este mapa de bits contendrá nuestra imagen final de la pieza del rompecabezas
                Bitmap puzzlePiece = Bitmap.createBitmap(piezaAncho + offsetX, piezaAlto + offsetY, Bitmap.Config.ARGB_8888);

                // dibuja la ruta
                int bumpSize = piezaAlto / 4;
                Canvas canvas = new Canvas(puzzlePiece);
                Path path = new Path();
                path.moveTo(offsetX, offsetY);
                if (fila == 0) {
                    // pieza lateral superior
                    path.lineTo(pieceBitmap.getWidth(), offsetY);
                } else {
                    // defarmación de la parte de arriba de la pieza
                    path.lineTo(offsetX + (pieceBitmap.getWidth() - offsetX) / 3, offsetY);
                    path.cubicTo(offsetX + (pieceBitmap.getWidth() - offsetX) / 6, offsetY - bumpSize, offsetX + (pieceBitmap.getWidth() - offsetX) / 6 * 5, offsetY - bumpSize, offsetX + (pieceBitmap.getWidth() - offsetX) / 3 * 2, offsetY);
                    path.lineTo(pieceBitmap.getWidth(), offsetY);
                }

                if (col == columnas - 1) {
                    // pieza lateral derecha
                    path.lineTo(pieceBitmap.getWidth(), pieceBitmap.getHeight());
                } else {
                    // defarmación de la parte derecha de la pieza
                    path.lineTo(pieceBitmap.getWidth(), offsetY + (pieceBitmap.getHeight() - offsetY) / 3);
                    path.cubicTo(pieceBitmap.getWidth() - bumpSize, offsetY + (pieceBitmap.getHeight() - offsetY) / 6, pieceBitmap.getWidth() - bumpSize, offsetY + (pieceBitmap.getHeight() - offsetY) / 6 * 5, pieceBitmap.getWidth(), offsetY + (pieceBitmap.getHeight() - offsetY) / 3 * 2);
                    path.lineTo(pieceBitmap.getWidth(), pieceBitmap.getHeight());
                }

                if (fila == filas - 1) {
                    // pieza lateral inferior
                    path.lineTo(offsetX, pieceBitmap.getHeight());
                } else {
                    // Deformación de la parte inferior de la pieza
                    path.lineTo(offsetX + (pieceBitmap.getWidth() - offsetX) / 3 * 2, pieceBitmap.getHeight());
                    path.cubicTo(offsetX + (pieceBitmap.getWidth() - offsetX) / 6 * 5, pieceBitmap.getHeight() - bumpSize, offsetX + (pieceBitmap.getWidth() - offsetX) / 6, pieceBitmap.getHeight() - bumpSize, offsetX + (pieceBitmap.getWidth() - offsetX) / 3, pieceBitmap.getHeight());
                    path.lineTo(offsetX, pieceBitmap.getHeight());
                }

                if (col == 0) {
                    // pieza laterla izquierda
                    path.close();
                } else {
                    // Deformación de la parte izquierda de la pieza
                    path.lineTo(offsetX, offsetY + (pieceBitmap.getHeight() - offsetY) / 3 * 2);
                    path.cubicTo(offsetX - bumpSize, offsetY + (pieceBitmap.getHeight() - offsetY) / 6 * 5, offsetX - bumpSize, offsetY + (pieceBitmap.getHeight() - offsetY) / 6, offsetX, offsetY + (pieceBitmap.getHeight() - offsetY) / 3);
                    path.close();
                }
               enmascara(canvas, path, pieceBitmap);
                bordeBlancoNegro(canvas, path);
                piece.setImageBitmap(puzzlePiece);
                piezas.add(piece);
                xCoord += piezaAncho;
            }
            yCoord += piezaAlto;
        }

        return piezas;
    }

    //Pinta el borde de las piezas recortadas
    public void bordeBlancoNegro(Canvas canvas, Path path) {
        Paint border = new Paint();
        border.setColor(0X80FFFFFF);
        border.setStyle(Paint.Style.STROKE);
        border.setStrokeWidth(8.0f);
        canvas.drawPath(path, border);

        border = new Paint();
        border.setColor(0X80000000);
        border.setStyle(Paint.Style.STROKE);
        border.setStrokeWidth(3.0f);
        canvas.drawPath(path, border);

    }

    // Muestra la imagen en las piezas recortadas y en forma de puzzle , sin esta clase solo se ven piezas de puzzle sin mmagenes
    public void enmascara(Canvas canvas, Path path, Bitmap pieceBitmap) {
        Paint paint = new Paint();
        paint.setColor(0XFF000000);
        paint.setStyle(Paint.Style.FILL);

        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(pieceBitmap, 0, 0, paint);
    }

    //Obtener las dimensiones de la imagen
    private int[] getBitmapPositionInsideImageView(ImageView imageView) {
        int[] ret = new int[4];

        if (imageView == null || imageView.getDrawable() == null)
            return ret;

        //Obtener los valores de la matriz de la imagen y colocarlos en un array
        float[] f = new float[9];
        imageView.getImageMatrix().getValues(f);

        // Extraer los valores de escala utilizando las constantes (para mantener  la relación de aspecto, scaleX == scaleY)
        final float scaleX = f[Matrix.MSCALE_X];
        final float scaleY = f[Matrix.MSCALE_Y];

        // Obtener el dibujo de la imagen mediante el objeto Drawable
        final Drawable d = imageView.getDrawable();
        final int origW = d.getIntrinsicWidth();
        final int origH = d.getIntrinsicHeight();

        // Calcula las dimensiones actuales
        final int actW = Math.round(origW * scaleX);
        final int actH = Math.round(origH * scaleY);

        ret[2] = actW;
        ret[3] = actH;

        //  Obtener la posición de la imagen
        int imgViewW = imageView.getWidth();
        int imgViewH = imageView.getHeight();

        int top = (int) (imgViewH - actH) / 2;
        int left = (int) (imgViewW - actW) / 2;

        ret[0] = left;
        ret[1] = top;

        return ret;
    }

    //Inicia la cuenta atrás cuando comienzas el puzzle
    private class Contador extends CountDownTimer {
        final TextView textView = (TextView) findViewById(R.id.counttime);
        final TextView gameover = findViewById(R.id.gameover);

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public Contador(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            textView.setText(String.format(Locale.getDefault(), "%d sec  ", millisUntilFinished / 1000L));

        }

        @Override
        public void onFinish() {
            gameover.setText("Game Over");
            gameOver();

        }
    }

    // Este método agrega un evento al calendario si se ha producido un récord
    // y en ese caso además envía una notificación.
    void agregarEventoCalendario(int nivel, double segTranscurridos) {

        //Evalua los registros que existen en el calendario mediante la función recuperarPuntuaciones
        if (recurperarPuntuacionesCalendario(nivel, segTranscurridos)) {
            long fecha_record = System.currentTimeMillis();
            ContentResolver cr = getContentResolver();
            ContentValues values = new ContentValues();

            nivel = (int) Math.sqrt(nivel);
            String myString = NumberFormat.getInstance().format(segTranscurridos);
            values.put(CalendarContract.Events.DTSTART, fecha_record);
            values.put(CalendarContract.Events.DTEND, fecha_record);
            values.put(CalendarContract.Events.TITLE, getString(R.string.nuevo_record) + nivel + "!");
            values.put(CalendarContract.Events.DESCRIPTION, myString);
            values.put(CalendarContract.Events.CALENDAR_ID, 3);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, getString(R.string.jugando));

            // Comprueba los permisos de acceso al calendario
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

            // Envía notificación al canal
            crearCanalNotificacion();

            String record = getString(R.string.record);
            @SuppressLint("DefaultLocale") NotificationCompat.Builder builder = new NotificationCompat.Builder(Juego.this, "CHANNEL_NEW_RECORD")
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle("App2Puzzle")
                    .setContentText(record + String.format("%.2f", segTranscurridos).replace(".", ",") + "s");

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(Juego.this);

            notificationManager.notify(1, builder.build());
        }

    }

    //  Comprueba si exixten registros en la aplicación del calendario
    private boolean recurperarPuntuacionesCalendario(int nivel, double segTranscurridos) {
        ContentResolver contentResolver = getContentResolver();
        Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2000, Calendar.JANUARY, 1, 0, 0);
        long startMills = beginTime.getTimeInMillis();
        long endMills = System.currentTimeMillis();

        ContentUris.appendId(builder, startMills);
        ContentUris.appendId(builder, endMills);
        String[] args = new String[]{"3"};

        Cursor eventCursor = contentResolver.query(builder.build(), new String[]{CalendarContract.Instances.TITLE,
                        CalendarContract.Instances.BEGIN, CalendarContract.Instances.END, CalendarContract.Instances.DESCRIPTION},
                CalendarContract.Instances.CALENDAR_ID + " = ?", args, null);

        boolean isRecord = false;
        boolean hayRegistros = false;

        while (eventCursor.moveToNext()) {
            final String title = eventCursor.getString(0);
            final String description = eventCursor.getString(3);

            if (title.length() == 22 && title.substring(title.length() - 2, title.length() - 1).equals(Integer.toString(nivel))) {
                hayRegistros = true;
                if (segTranscurridos < Double.parseDouble(description.replace(",", "."))) {
                    isRecord = true;
                } else {
                    isRecord = false;
                }
            }
        }

        if (hayRegistros) {
            return isRecord;
        } else {
            return !isRecord;
        }
    }

    // Crea el canal de notificaciones .
    private void crearCanalNotificacion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("CHANNEL_NEW_RECORD", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    //Evalua si el puzzle está completo para parar el contador y volver a selecciona imagen
    public void pasaNivel() {

        if (puzzleCompleto()) {
            mpContador.setLooping(false);
            mpContador.stop();
            mp = MediaPlayer.create(this, R.raw.aplausos);
            mp.start();
            finish();
            //ranking();
            contador.cancel();
        }
    }
    //Comprueba si se pueden mover piezas y el nivel del juego
    private boolean puzzleCompleto() {

        for (ColocaPiezas piece : piezas) {
            if (piece.canMove) {
                mp = MediaPlayer.create(this, R.raw.pieza_encaja);
                mp.start();
                return false;
            }
        }



        tiempoFin = System.currentTimeMillis();
        sdf = new SimpleDateFormat(patronFecha);
        fechaActual = sdf.format(new Date(tiempoFin));
        tiempoDiferencia = tiempoFin - tiempoInicio;
        segTranscurridos = tiempoDiferencia / 1000.0;
        agregarEventoCalendario(numeroPiezas,segTranscurridos );
        return true;
    }

    //Contdor finlizado partida perdida
    private void gameOver() {
        finish();
        mpContador.setLooping(false);
        mpContador.stop();
        contador.cancel();
        mp = MediaPlayer.create(this, R.raw.game_over);
        mp.start();
        Intent intent = new Intent(this, Game_over.class);
        startActivity(intent);
    }

    //Si el puzzle está completo envía los registros a la base de datos
    public void ranking() {
        if (puzzleCompleto()) {
            numeroPiezas = (int) Math.sqrt(numeroPiezas);
            if (numeroPiezas == 3) {
                numeroPiezas = 1;
            } else if (numeroPiezas == 4) {
                numeroPiezas = 2;
            } else if (numeroPiezas == 5) {
                numeroPiezas = 3;
            }
            final BDHelper dbHelper = new BDHelper(this);
            SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

            // Insertamos la puntuación en la BBDD
            ContentValues valores = new ContentValues();

            valores.put(BBDDEsquema.NOMBRE,nombre);
            valores.put(BBDDEsquema.FECHA, String.valueOf(fechaActual));
            valores.put(BBDDEsquema.NIVEL, numeroPiezas);
            valores.put(BBDDEsquema.PUNTOS, segTranscurridos);

            long idNuevaFila = sqLiteDatabase.insert(BBDDEsquema.NOM_TABLA, null, valores);

        }

    }

    //Action Bar que muestra la activación y desactivación del audio
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.sonidosOff:
                //soundPool.play(seleccionPuzzle,5,5,1,0,1);
                reproduciendo = !item.isChecked();
                this.setFavoriteIcon(item);
                item.setChecked(reproduciendo);
                if (reproduciendo) {
                    AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    findViewById(R.id.sonidosOff).setVisibility(View.VISIBLE);
                } else {
                    AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                }
                return true;
            default:

                return super.onOptionsItemSelected(item);
        }
    }

    //Función que cambia el estado del icono en función de si está encendida o apagada
    private void setFavoriteIcon(MenuItem item) {
        int id = this.reproduciendo ? R.drawable.ic_baseline_music_off_24 : R.drawable.ic_baseline_music_note_24;
        item.setIcon(ContextCompat.getDrawable((Context)this, id));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.imagenes, menu);
        return true;
    }

}




























