package com.example.puzzlen;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

public class ColocaPiezas extends AppCompatImageView {
    public int xCoord;
    public int yCoord;
    public int pieceWidth;
    public int pieceHeight;
    public boolean canMove = true;
    public ColocaPiezas(@NonNull Context context) {

        super(context);
    }
}
