package com.example.puzzlen;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class MuevePiezas implements View.OnTouchListener{
    private float xDelta;
    private float yDelta;
    private Juego juego;

    public MuevePiezas(Juego juego ){
        this.juego=juego;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getRawX();
        float y = event.getRawY();
        ColocaPiezas piece = (ColocaPiezas) v;

        final double tolerance = sqrt(pow(v.getWidth(), 2) + pow(v.getHeight(), 2)) / 10;
        if (!piece.canMove) {
            return true;
        }
        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                MediaPlayer mp;
                mp= MediaPlayer.create(juego.getApplicationContext(), R.raw.arrastre);
                mp.start();

                xDelta = x - lParams.leftMargin;
                yDelta = y - lParams.topMargin;
                piece.bringToFront();
                break;
            case MotionEvent.ACTION_MOVE:
                lParams.leftMargin = (int) (x - xDelta);
                lParams.topMargin = (int) (y - yDelta);
                v.setLayoutParams(lParams);
                break;
            case MotionEvent.ACTION_UP:
                int xDiff = abs(piece.xCoord - lParams.leftMargin);
                int yDiff = abs(piece.yCoord - lParams.topMargin);
                if (xDiff <= tolerance && yDiff <= tolerance) {
                    lParams.leftMargin = piece.xCoord;
                    lParams.topMargin = piece.yCoord;
                    piece.setLayoutParams(lParams);
                    piece.canMove = false;
                    sendViewToBack(piece);
                    juego.pasaNivel();
                    juego.ranking();

                }
                break;
        }

        return true;
    }
    private void sendViewToBack(ColocaPiezas piece) {
        final ViewGroup parent = (ViewGroup)piece.getParent();
        if (null != parent) {
            parent.removeView(piece);
            parent.addView(piece, 0);
        }

    }
}
