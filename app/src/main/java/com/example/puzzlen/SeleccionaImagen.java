package com.example.puzzlen;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class SeleccionaImagen extends AppCompatActivity implements AdapterView.OnClickListener {
    private static final int REQUEST_PERMISSION_CAMERA = 100;
    private static final int REQUEST_IMAGE_CAMERA = 1;
    private static final int REQUEST_PERMISSION_WRITE_STORAGE = 200;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;


    static String datos;
    private boolean reproduciendo=false;
    int f = 0;
    String rutaImagen;
    GridView gridView;
    private boolean favorito = false;
    ControlMusica controlVolumen;

    private ArrayList<Integer> imagenesDisponibles = new ArrayList<>();
    private ArrayList<Integer> imagenesUsadas = new ArrayList<>();
    private Integer REQUEST_CAMERA = 1;
    private int imagen;
    Intent music;
    private boolean mIsBound = false;
    private ServicioMusica mServ;
    Uri imagenUri;

    List<Uri> listaImagenes = new ArrayList<>();
    Adaptador adaptador;
    List<Uri> listaImagenesRecuperadas = new ArrayList<>();
    private SoundPool soundPool;
    int seleccionPuzzle;
    int seleccionaGaleria;
    int seleccionaCamara;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selecciona_imagen);

        imagenesGrid();

        datosJugador();


       // Button btnPlay = (Button) findViewById(R.id.sonidoOn);
       // Button btnStop = (Button) findViewById(R.id.sonidoOff);
        Button tres = (Button) findViewById(R.id.tres);
        Button cuatro = (Button) findViewById(R.id.cuatro);
        Button cinco = (Button) findViewById(R.id.cinco);
        Button camera = (Button) findViewById(R.id.camera);
        Button bttGaleriaImagen = (Button) findViewById(R.id.imagenGaleria);

        //btnPlay.setOnClickListener(this);
        //btnStop.setOnClickListener(this);
        tres.setOnClickListener(this);
        cuatro.setOnClickListener(this);
        cinco.setOnClickListener(this);
        camera.setOnClickListener(this);
        bttGaleriaImagen.setOnClickListener(this);

        seleccionaNuevaPartida();

        sonidos();

        doBindService();
        music = new Intent();
        music.setClass(this, ServicioMusica.class);
        startService(music);

        volumenSegundoPlano();
    }

    //Función que para la música cuando el juego no es la actividad principal
    private void volumenSegundoPlano(){
        controlVolumen = new ControlMusica(this);
        controlVolumen.setOnHomePressedListener(new ControlMusica.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                if (mServ != null) {
                    mServ.pauseMusic();
                }
            }

            @Override
            public void onHomeLongPressed() {
                if (mServ != null) {
                    mServ.pauseMusic();
                }
            }
        });
        controlVolumen.startWatch();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                abrirCamera();
            } else {
                Toast.makeText(SeleccionaImagen.this, "Necesitas habilitar los permisos", Toast.LENGTH_LONG).show();
            }
            //El siguiente else es de Juan
        } else if (requestCode == REQUEST_PERMISSION_WRITE_STORAGE) {
            if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(this, "Necesitas habilitar los permisos de la galería", Toast.LENGTH_LONG).show();
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context, Manifest.permission.READ_EXTERNAL_STORAGE);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAMERA && resultCode == RESULT_OK) {
            File file = new File(rutaImagen);
            if (file.exists()) {
                Uri manolo = Uri.fromFile(new File(String.valueOf(file)));
                listaImagenesRecuperadas.add(manolo);

            }

        }
        adaptador = new Adaptador(SeleccionaImagen.this, listaImagenesRecuperadas);
        gridView.setAdapter(adaptador);
    }

    public void onClick(View v) {

        //Comprobamos el identificador del boton que ha llamado al evento para ver
        //cual de los botones es y ejecutar la acción correspondiente

        switch (v.getId()) {

            case R.id.tres:
                soundPool.play(seleccionPuzzle,1,1,1,0,1);
                f = 3;
                imagenesGrid();
                break;
            case R.id.cuatro:
                soundPool.play(seleccionPuzzle,1,1,1,0,1);
                f = 4;
                imagenesGrid();
                break;
            case R.id.cinco:
                soundPool.play(seleccionPuzzle,1,1,1,0,1);
                f = 5;
                imagenesGrid();
                break;
            case R.id.camera:
                soundPool.play(seleccionaCamara,1,1,1,0,1);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ActivityCompat.checkSelfPermission(SeleccionaImagen.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        abrirCamera();
                    } else {
                        ActivityCompat.requestPermissions(SeleccionaImagen.this, new String[]{Manifest.permission.CAMERA}, 100);
                    }
                } else {
                    abrirCamera();
                }
                break;

            case R.id.imagenGaleria:
                soundPool.play(seleccionaGaleria,5,5,1,0,1);
                abrirGaleria();
                break;
        }
    }

    public void imagenesGrid() {

        gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new Adaptador(this, listaImagenes));

        if (!(findViewById(R.id.tres).isSelected() || findViewById(R.id.cuatro).isSelected() || findViewById(R.id.cinco).isSelected())) {

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Intent intent = new Intent(getApplicationContext(), Juego.class);
                    intent.putExtra("milista", (Serializable) listaImagenesRecuperadas);
                    intent.putExtra("id", position);
                    intent.putExtra("filas", f);
                    intent.putExtra("nombreJugador", datos);

                    mServ.onDestroy();
                    startActivity(intent);



                }
            });

        } else {
            Toast.makeText(this, "Selecciona un tipo de puzzle", Toast.LENGTH_LONG).show();
        }

    }

    public void abrirGaleria() {
        try {
            Thread.sleep(1200);
        } catch (Exception exception){
            System.out.println(exception);
        }

        if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
            ContentResolver cr = getApplicationContext().getContentResolver();
            String[] projection = new String[]{MediaStore.Images.Media._ID};

            Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String id = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID));
                    imagenUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id));
                    imagenesDisponibles.add(Integer.parseInt(id));
                }
                cursor.close();
            }
            for (int i = 0; i < 3; i++) {
                imagen = seleccionarImagenAleatoria(imagenesDisponibles);
                String recuperaImagen = String.valueOf(imagen);
                imagenUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, recuperaImagen);

                listaImagenesRecuperadas.add(imagenUri);

            }
            adaptador = new Adaptador(SeleccionaImagen.this, listaImagenesRecuperadas);
            gridView.setAdapter(adaptador);
        }

    }

    public void showDialog(final String msg, final Context context, final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{permission},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    private int seleccionarImagenAleatoria(ArrayList<Integer> imagenes) {
        Random rand = new Random();
        int imagen = imagenes.get(rand.nextInt(imagenes.size()));

        while (imagenesUsadas.contains(imagen)) {
            imagen = imagenes.get(rand.nextInt(imagenes.size()));
        }

        imagenesUsadas.add(imagen);
        return imagen;
    }

    public void datosJugador() {
        TextView nombre = (TextView) findViewById(R.id.elige);
        Bundle parametros = this.getIntent().getExtras();
        datos = parametros.getString("nombrejugador");
        if (datos != null) {
            nombre.setText("Hola, " + datos + "! Elige un Rompecabezas!");
        } else {
            nombre.setText("Hola de nuevo! Elige otro Rompecabezas!");
        }
    }

    public void seleccionaNuevaPartida() {

        Bundle extras = getIntent().getExtras();
        boolean callMethod = extras.getBoolean("imagenesGrid");
        if (callMethod) {
            imagenesGrid();
        }
    }

    private void abrirCamera() {
        //mp= MediaPlayer.create(this,R.raw.camara_abriendo);
        //mp.start();
        try {
            Thread.sleep(1200);
        } catch (Exception exception){
            System.out.println(exception);
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //if(intent.resolveActivity(getPackageManager()) != null) {
        File fotoFile = null;
        try {
            fotoFile = creaImagen();
        } catch (IOException ex) {

        }
        if (fotoFile != null) {
            Uri fotoUri = FileProvider.getUriForFile(getApplicationContext(), "com.example.android.fileprovider", fotoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fotoUri);
            startActivityForResult(intent, REQUEST_IMAGE_CAMERA);
        }
        //}
    }

    private File creaImagen() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "App2Puzzle_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File imagen = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        rutaImagen = imagen.getAbsolutePath();
        return imagen;

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Añadimos el checkbox para encender y apagar la música de fondo.
        MenuItem checkable = menu.findItem(R.id.sonidosOff);

        checkable.setChecked(reproduciendo);
        return true;
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.sonidosOff:
                soundPool.play(seleccionPuzzle,5,5,1,0,1);
                reproduciendo = !item.isChecked();
                this.setFavoriteIcon(item);
                item.setChecked(reproduciendo);
                if (reproduciendo) {
                    AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    findViewById(R.id.sonidosOff).setVisibility(View.VISIBLE);
                } else {
                    AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                }
                return true;
            default:

                return super.onOptionsItemSelected(item);
        }
    }
    private void setFavoriteIcon(MenuItem item) {

        int id = this.reproduciendo ? R.drawable.ic_baseline_music_off_24 : R.drawable.ic_baseline_music_note_24;
        item.setIcon(ContextCompat.getDrawable((Context)this, id));
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.imagenes, menu);
        return true;
    }

    // Este método reanuda la música
    @Override
    protected void onResume() {
        super.onResume();

        if (mServ != null) {
            mServ.resumeMusic();
        }
    }

    // Este método pone la música en pausa
    @Override
    protected void onPause() {
        super.onPause();

        // Detectamos la pausa de la pantalla
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = false;
        if (pm != null) {
            isScreenOn = pm.isScreenOn();
        }

        if (!isScreenOn) {
            if (mServ != null) {
                mServ.pauseMusic();
            }
        }
    }

    // Este método desvincula el servicio de música cuando no lo necesitamos
    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Desvinculamos el servicio de música
        doUnbindService();
        Intent music = new Intent();
        music.setClass(this,ServicioMusica.class);
        stopService(music);
    }


    // Vinculamos el servicio de música

    private ServiceConnection Scon = new ServiceConnection() {

        public void onServiceConnected(ComponentName name, IBinder binder) {
            mServ = ((ServicioMusica.ServiceBinder) binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    // Vincular servicio
    void doBindService() {
        bindService(new Intent(this, ServicioMusica.class), Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    // Desvincular servicio
    void doUnbindService() {
        if (mIsBound) {
            unbindService(Scon);
            mIsBound = false;
        }
    }

    private void sonidos(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(6)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        }

        seleccionPuzzle = soundPool.load(this, R.raw.click, 1);
        seleccionaCamara = soundPool.load(this, R.raw.camara_abriendo, 1);
        seleccionaGaleria = soundPool.load(this, R.raw.galeria, 1);
    }

}

  /* case R.id.sonidoOn:
                soundPool.play(seleccionPuzzle,5,5,1,0,1);
                if (!reproduciendo) {//verifica que ya no se este reproduciendo
                    mp = MediaPlayer.create(this, R.raw.musica);
                    //mp.stop();
                    mp.setLooping(true);
                    mp.start();
                    reproduciendo = true;

                    findViewById(R.id.sonidoOn).setVisibility(View.GONE);
                    findViewById(R.id.sonidoOff).setVisibility(View.VISIBLE);

                    onResume();
                    setVolumeControlStream(AudioManager.STREAM_MUSIC);
                }
                break;

            case R.id.sonidoOff:
                //Paramos el audio y volvemos a inicializar
                soundPool.play(seleccionPuzzle,5,5,1,0,1);
                if (reproduciendo) {
                    mp.stop();
                    mp.setLooping(false);
                    reproduciendo = false;

                    findViewById(R.id.sonidoOn).setVisibility(View.VISIBLE);
                    findViewById(R.id.sonidoOff).setVisibility(View.GONE);
                }
                break;*/

//mp = MediaPlayer.create(this, R.raw.click);
//mp.start();