package com.example.puzzlen;

public class BBDDEsquema {
    private BBDDEsquema(){

    }
    static final String NOM_TABLA = "Ranking";
    private static final String _ID ="id";
    static final String NOMBRE ="nombre";
    static final String FECHA ="fecha";
    static final String NIVEL ="nivel";
    static final String PUNTOS = "puntos";

    static final String SQL_INSERT =
            " CREATE TABLE " + BBDDEsquema.NOM_TABLA + " ( " +
                    BBDDEsquema._ID + " INTEGER PRIMARY KEY," +
                    BBDDEsquema.NOMBRE + " TEXT," +
                    BBDDEsquema.FECHA + " TEXT," +
                    BBDDEsquema.NIVEL + " INT," +
                    BBDDEsquema.PUNTOS + " FLOAT)";

    static final String SQL_DELETE =
            "DROP TABLE IF EXISTS " + NOM_TABLA;

}
